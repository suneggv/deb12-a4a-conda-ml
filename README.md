# *Oriolus* -- Python ML dev docker images

## Currently includes
* Debian 12
* Anaconda *(running Python 3.11)*
* `mesa-utils` and Intel GPU support *(AMD and Nvidia coming soon)*
* ***A4A conda environment*** (`conda activate A4A`)
    * `pyaudio`
    * `pydub`

* ***TensorFlow CPU + Farama Gymnasium conda environment*** (`conda activate RL_TF_K_CPU`)
    * `tensorflow` + `keras` + `tensorboard` *(CPU-only version for now)*
    * most of `gymnasium[all]`, but excluding the things from the `other` channel *(namely the ones with CUDA dependencies)*
    * `opencv-python`

* ***PyTorch CPU + Farama Gymnasium conda environment*** (`conda activate RL_PT_T_CPU`)
    * `torch`+ `torchvision` + `torchaudio` *(CPU-only version for now)*
    * `tensorboard`
    * `pytorch-ignite`
    * most of `gymnasium[all]`, but excluding the things from the `other` channel *(namely the ones with CUDA dependencies)*
    * `opencv-python`

* ***Suggested `docker run` command exposes the following host files/devices:***
    * 3 host folders
    * Host sound and display devices
    * Host GPU *(currently just Intel, but soon also AMD and Nvidia)*

## Getting started
### Building the dockerfile
* Clone `Dockerfile` and `startup.sh` to a folder
* In that folder, run `docker build -t <IMAGE NAME GOES HERE> .` *(don't forget the dot there)*
* This will have generated a docker image with the name you gave


### Running the docker image
* Create three folders. Call them `~/cont_shr1`, `~/cont_shr2`, and `~/cont_shr3`
* Run this command below:
    * `docker run -it -v ~/cont_shr1:/opt/shared_folders/shr1 -v ~/cont_shr2:/opt/shared_folders/shr2 -v ~/cont_shr3:/opt/shared_folders/shr3 --device /dev/snd -e PULSE_SERVER=unix:${XDG_RUNTIME_DIR}/pulse/native -v ${XDG_RUNTIME_DIR}/pulse/native:${XDG_RUNTIME_DIR}/pulse/native -v ~/.config/pulse/cookie:/root/.config/pulse/cookie --group-add $(getent group audio| cut -d: -f3) -e GID=$(id -g) -e UID=$(id -u) -e USER=$USER --net=host -e DISPLAY -v ${HOME}/.Xauthority:/home/user/.Xauthority --device /dev/dri/card0 --device /dev/dri/renderD128 <IMAGE NAME GOES HERE>`
* This will have created a new container based on the docker image with the name you gave. You will be able to access the folders `~/cont_shr1`, `~/cont_shr2`, and `~/cont_shr3` from the container at `/opt/shared_folders/`


### Setting up conda inside the container
* Inside the container, run the following:
    * `conda init`
    * `exit`
    * do `docker ps -a`, to see the ID of the container you just exited
    * `docker start <CONTAINER ID GOES HERE>`
    * `docker exec -it <CONTAINER ID GOES HERE> runuser -u <YOUR USERNAME> /bin/bash`
    * ***To run the base environment:***
        * Do nothing. You're already in the base environment
    * ***To run the A4A environment:***
        * `conda activate A4A`
    * ***To run the TensorFlow CPU + Farama Gymnasium environment:***
        * `conda activate RL_TF_K_CPU`
    * ***To run the PyTorch CPU + Farama Gymnasium environment:***
        * `conda activate RL_PT_T_CPU`

## Running the container                                                                                                                                                                                                                                                                                                                                                                                                                 

### "Logging in" to the container

* ***As root***
    * `docker exec -it <CONTAINER ID GOES HERE> /bin/bash`

* ***As yourself** (you will likely have to do this if you want to display a wwindow from the container)*
    * `docker exec -it <CONTAINER ID GOES HERE> runuser -u <YOUR USERNAME> /bin/bash`


### Running jupyter
`jupyter notebook --ip=127.0.0.1 --port=8888 --allow-root --notebook-dir=/opt/shared_folders`


### Running tensorboard on the `RL_TF_K_CPU` environment
* ***In the python file, add this***
    * In the imports:
        * *`import datetime`*
    * Before `model.fit`:
        * *`log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")`*
        * *`tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)`*
    * Inside `model.fit()`, give the argument `callbacks=[tensorboard_callback]`
* `python the_python_file_you_added_things_to.py`
* `tensorboard --logdir=logs/fit --port=6006`
* open a browser on `https://localhost:6006`


### Running tensorboard on the `RL_PT_T_CPU` environment

* ***In the python file, add this:***
    * In the imports:
        * *`from torch.utils.tensorboard import SummaryWriter`*
    * In the model training function, at each epoch:
        * *`writer.add_scalar("Loss/train", loss, epoch)`*
    * At the end of the python file:
        * *`writer.flush()`*
* When you run the python file, it will now record a log of the training to the `runs` folder, which will have been created in the same directory as the python file
    * `python the_python_file_you_added_things_to.py`
    * `tensorboard --logdir=logs/fit --port=6006`
    * open a browser on `https://localhost:6006`