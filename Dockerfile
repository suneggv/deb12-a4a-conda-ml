FROM buildpack-deps:bookworm

# == Anaconda stuff ============================================================

# ensure conda python is preferred over distribution python
ENV PATH /opt/conda/bin:$PATH

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8

# get system up to date
RUN set -eux; \
	apt-get update; \
	apt-get upgrade

# runtime dependencies
RUN set -eux; \
	apt-get install -y bzip2 ca-certificates git libglib2.0-0 libsm6 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxinerama1 libxrandr2 libxrender1 mercurial openssh-client procps subversion wget

# download (URL MIGHT CHANGE IN THE FUTURE)
RUN set -eux; \
	wget "https://repo.anaconda.com/archive/Anaconda3-2023.09-0-Linux-x86_64.sh" -O anaconda.sh -q

# checksum (SHA256 SUM MIGHT CHANGE IN THE FUTURE)
RUN set -eux; \
	echo "6c8a4abb36fbb711dc055b7049a23bbfd61d356de9468b41c5140f8a11abd851 anaconda.sh" > shasum; \
	sha256sum --check --status shasum

# install
RUN set -eux; \
	/bin/bash anaconda.sh -b -p /opt/conda

# housekeeping
RUN set -eux; \
	rm anaconda.sh shasum; \
	ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh; \
	echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc; \
	echo "conda activate base" >> ~/.bashrc; \
	find /opt/conda/ -follow -type f -name '*.a' -delete; \
	find /opt/conda/ -follow -type f -name '*.js.map' -delete

# preemptively clean up conda environment
RUN set -eux; \
	/opt/conda/bin/conda clean -afy

# create the necessary conda environments
SHELL ["/bin/bash", "--login", "-c"]
RUN set -eux; \
	conda create --name A4A --clone base; \
	conda create --name RL_TF_K_CPU --clone A4A; \
	conda create --name RL_PT_T_CPU --clone RL_TF_K_CPU; \
	conda init bash


# == A4A stuff =================================================================

# ALSA
RUN set -eux; \
	apt-get -y install libasound2 libasound2-plugins alsa-utils alsa-oss apulse

# Pulseaudio
RUN set -eux; \
	apt-get -y install pulseaudio

# Portaudio
RUN set -eux; \
	apt-get -y install portaudio19-dev libportaudio2 libportaudiocpp0

# ffmpeg
RUN set -eux; \
	apt-get -y install ffmpeg

# clone base environment to an A4A-specific environment, switch to it
RUN echo "conda activate A4A" >> ~/.bashrc

# install pyaudio and pydub to (A4A), then exit out of (A4A)
RUN set -eux; \
	pip install pyaudio pydub; \
	conda deactivate; \
	head -n -1 ~/.bashrc > ~/.bashrc.tmp ; mv ~/.bashrc.tmp ~/.bashrc

# install net-tools
RUN set -eux; \
	apt-get -y update && apt-get -y install net-tools

# libraries that the FBE visualization needs to run
RUN set -eux; \
	apt-get -y update && apt-get -y install libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-render-util0


# == TensorFlow CPU (RL_TF_K_CPU) ==============================================

# switch to RL_TF_K_CPU environment
RUN echo "conda activate RL_TF_K_CPU" >> ~/.bashrc

# install tensorflow to (RL_TF_K_CPU), then exit out of (RL_TF_K_CPU)
RUN set -eux; \
	pip install tensorflow; \
	pip install swig opencv-python; \
	pip install gymnasium[atari,box2d,classic-control,mujoco,py,mujoco,toy-text,jax]; \
	conda deactivate; \
	head -n -1 ~/.bashrc > ~/.bashrc.tmp ; mv ~/.bashrc.tmp ~/.bashrc


# == PyTorch CPU (RL_PT_T_CPU) =================================================

# install graphics stack libraries and utilities
RUN set -eux; \
	apt-get -y install mesa-utils libgl1-mesa-glx

# switch to RL_PT_T_CPU environment
RUN echo "conda activate RL_PT_T_CPU" >> ~/.bashrc

# install PyTorch to (RL_PT_T_CPU), then exit out of (RL_PT_T_CPU)
RUN set -eux; \
	pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu; \
	pip install tensorboard fire; \
	pip install pytorch-ignite; \
	pip install swig opencv-python; \
	pip install gymnasium[atari,box2d,classic-control,mujoco,py,mujoco,toy-text,jax]; \
	conda deactivate; \
	head -n -1 ~/.bashrc > ~/.bashrc.tmp ; mv ~/.bashrc.tmp ~/.bashrc


# == Preparing Entrypoint ======================================================

COPY startup.sh /root/
ENTRYPOINT ["/root/startup.sh"]